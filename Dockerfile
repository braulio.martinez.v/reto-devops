FROM node:carbon-alpine
CMD [MKDIR, /DOCKERS]   
WORKDIR /DOCKERS
COPY index.js /DOCKERS
COPY package*.json /DOCKERS
RUN npm install
EXPOSE 3000
CMD node index.js